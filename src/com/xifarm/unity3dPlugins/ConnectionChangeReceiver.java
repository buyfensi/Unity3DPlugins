package com.xifarm.unity3dPlugins;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class ConnectionChangeReceiver  extends BroadcastReceiver {

	private ExDataListener mExDataListener;
	public void SetListener(ExDataListener listen)
	{
	    this.mExDataListener = listen;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		ConnectivityManager connectivityManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo  mobNetInfo=connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo  wifiNetInfo=connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        
        if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
        	log("网络不可以用");
//            BSToast.showLong(context, );
            //改变背景或者 处理网络的全局变量
        }else if(mobNetInfo.isConnected()){
        	log("网络-mobNetInfo: " + GetNetworkInfo(mobNetInfo));
        	
        }
        else if(wifiNetInfo.isConnected()){
        	log("网络-wifiNetInfo: " + GetNetworkInfo(wifiNetInfo));          	
        }
	}
	
	private String GetNetworkInfo(NetworkInfo net)
	{
		return net.toString();
	}
	
	private void log(String info)
	{
		Log.d("Unity", info);
if(mExDataListener != null)
{
	mExDataListener.onReceive(info);
	}
	}

}
