﻿using UnityEngine;
using System.Collections;
using PathologicalGames;
using System.Collections.Generic;

/// <summary>
/// PoolManager的牌桌封装
/// </summary>
public class PoolManagerWrap
{
    #region PoolManagerWrap

    private PoolManagerWrap()
    {
    }
    private static PoolManagerWrap mSingle;
    public static PoolManagerWrap Single
    {
        get
        {
            if (mSingle == null)
            {
                mSingle = new PoolManagerWrap();
            }
            return mSingle;
        }
    }
    #endregion

    public SpawnPool GetPool(string poolName = "game")
    {
        if (!PoolManager.Pools.ContainsKey(poolName))
        {
            SpawnPool p = PoolManager.Pools.Create(poolName);
            p.poolName = poolName;
            p.dontReparent = true;
            return p;
        }

        return PoolManager.Pools[poolName];
    }

    private Dictionary<string, GameObject> prefabDic = new Dictionary<string, GameObject>();

    public GameObject GetPrefab(string prefabFullPath)
    {
        if (!prefabDic.ContainsKey(prefabFullPath))
        {
            GameObject go = Resources.Load(prefabFullPath) as GameObject;
            prefabDic.Add(prefabFullPath, go);
        }

        return prefabDic[prefabFullPath];
    }
}
