﻿/* ***********************************************
 * author :  CodePiao Dev Team
 * blog  :  http://www.CodePiao.com 
 * function: AndroidTest
 * ***********************************************/

using System;
using System.Collections.Generic;
//using CodePiao.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace CodePiao
{
    /// <summary>
    /// AndroidTest
    /// </summary>
    public class AndroidTest : MonoBehaviour
    {
        private AndroidJavaObject ajc;
        public void Start()
        {
            AndroidJNIHelper.debug = true;
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            ajc = jc.GetStatic<AndroidJavaObject>("currentActivity");
        }


        public void Update()
        {
            if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Backspace))
            {
                Application.Quit();
            }
        }


        private string info = "nothing to Display.";
        private void ResetInfo()
        {
            info = "nothing to Display.";
        }
        public void OnGUI()
        {
            if (!string.IsNullOrEmpty(info))
            {
                info = GUILayout.TextArea(info);
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(20);
            if (GUILayout.Button("Max(10,20)"))
            {
                info = "Max(10,20)=" + ajc.Call<int>("Max", new object[] { (int)10, (int)20 });
            }

            GUILayout.Space(20);
            if (GUILayout.Button("Vibrate(2s)"))
            {
                ajc.Call("Vibrate", new object[] { (long)2000 });
            }

            GUILayout.Space(20);
            if (GUILayout.Button("GetDeviceMole"))
            {
                info = ajc.Call<string>("GetDeviceMole");
            }

            GUILayout.Space(20);
            if (GUILayout.Button("StartActivity1"))
            {
                ajc.Call("StartActivity1", new object[] { "这是第No 1 Activity." });
            }

            GUILayout.Space(20);
            if (GUILayout.Button("StartActivity2"))
            {
                ajc.Call("StartActivity2", new object[] { "这是第No 2 Activity." });
            }

            GUILayout.Space(20);
            if (GUILayout.Button("ShowMessage2"))
            {
                ajc.Call("ShowMessage2");
            }

            GUILayout.Space(20);
            if (GUILayout.Button("ShowMessage3"))
            {
                ajc.Call("ShowMessage3", new object[] { info });
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Space(20);
            #region CallBack-AndroidJavaProxy
            if (GUILayout.Button(String.Format("{0:yyyy-MM-dd}", DateCallback.date)))
            {
                ajc.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    new AndroidJavaObject("android.app.DatePickerDialog", ajc,
                        new DateCallback(),
                        DateCallback.date.Year,
                        DateCallback.date.Month - 1,
                        DateCallback.date.Day).Call("show");
                }));
            }
            #endregion

            GUILayout.Space(20);
            #region CallBack-AndroidJavaProxy
            if (GUILayout.Button(String.Format("{0:HH-mm}", TimeCallback.date)))
            {
                ajc.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    new AndroidJavaObject("android.app.TimePickerDialog", ajc,
                        new TimeCallback(),
                        TimeCallback.date.Hour,
                        TimeCallback.date.Minute,
                        true).Call("show");
                }));
            }
            #endregion

            #region CallBack-Wifi\Network
            if (GUILayout.Button("BeginAction"))
            {
                ajc.Call("BeginAction", new object[] { ExDataListenerCallback });
            }

            if (GUILayout.Button("EndAction"))
            {
                ajc.Call("EndAction");
                mExDataListenerCallback = null;
                ResetInfo();
            }
            #endregion

            #region CallBack-Wifi\Network
            if (GUILayout.Button("BeginAction-UnitySendMessage"))
            {
                ajc.Call("BeginAction_UnitySendMessage");
            }

            if (GUILayout.Button("EndAction-UnitySendMessage"))
            {
                ajc.Call("EndAction_UnitySendMessage");
                ResetInfo();
            }
            #endregion
            
            GUILayout.EndHorizontal();
        }

        private ExDataListenerCallback mExDataListenerCallback = null;
        private ExDataListenerCallback ExDataListenerCallback
        {
            get
            {
                if (mExDataListenerCallback == null)
                { mExDataListenerCallback = new ExDataListenerCallback(this); }
                return mExDataListenerCallback;
            }

        }

        public void onReceive(String data)
        {
            info = data;
        }

        public void onReceiveByUnitySendMessage(string data)
        {
            info = data;
        }
    }

    #region CallBack--AndroidJavaProxy
    public class DateCallback : AndroidJavaProxy
    {
        public static DateTime date = System.DateTime.Now;
        public DateCallback()
            : base("android.app.DatePickerDialog$OnDateSetListener")
        {
        }

        void onDateSet(AndroidJavaObject view, int year, int monthOfYear, int dayOfMonth)
        {
            DateCallback.date = new DateTime(year, monthOfYear + 1, dayOfMonth);
        }
    }

    public class TimeCallback : AndroidJavaProxy
    {
        public static DateTime date = System.DateTime.Now;
        public TimeCallback()
            : base("android.app.TimePickerDialog$OnTimeSetListener")
        {
        }

        void onDateSet(AndroidJavaObject timerPicker, int hourOfDay, int minute)
        {
            TimeCallback.date = new DateTime(TimeCallback.date.Year, TimeCallback.date.Month,
                TimeCallback.date.Day, hourOfDay, minute, TimeCallback.date.Second);
        }
    }

    public class ExDataListenerCallback : AndroidJavaProxy
    {
        private AndroidTest mMain= null;
        public ExDataListenerCallback(AndroidTest main)
            : base("com.xifarm.unity3dPlugins.ExDataListener")
        {
            mMain = main;
        }

        public void onReceive(String data)
        {
            mMain.onReceive(data);
        }
    }
    #endregion
}